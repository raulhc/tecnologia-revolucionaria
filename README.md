# Tecnologia revolucionária

## Objetivo

Compartilhar conhecimento sobre utilização de tecnologia com segurança.

## Público alvo

Pessoas não iniciadas/técnicas de TI

## Como usar essa apresentação?

Acesse a pasta modelo e crie um arquivo html seguindo [essa](https://github.com/hakimel/reveal.js) documentação.
